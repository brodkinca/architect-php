<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tests\Traits;

/**
 * Test \BCA\Architect\Traits\DockerHelper
 */
class DockerHelperTest extends TraitTestCase
{
    /**
     * Setup tests
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->roboReflector = new \ReflectionClass($this->robo);
    }

    /**
     * Test dockerHelper() returns a new DockerHelper class
     *
     * @group dockerlib
     *
     * @return void
     */
    public function testDockerHelper()
    {
        $method = $this->roboReflector->getMethod('taskDockerRun');
        $method->setAccessible(true);
        $result = $method->invoke($this->robo, 'busybox')->run();

        $method = $this->roboReflector->getMethod('dockerHelper');
        $method->setAccessible(true);

        $this->assertThat(
            $method->invoke($this->robo, $result),
            $this->isInstanceOf('\BCA\Architect\Tasks\DockerHelper')
        );
    }

    /**
     * Test dockerInstance() returns a new DockerInstance class
     *
     * @return void
     */
    public function testDockerInstance()
    {
        $method = $this->roboReflector->getMethod('dockerInstance');
        $method->setAccessible(true);

        $this->assertThat(
            $method->invoke($this->robo),
            $this->isInstanceOf('\BCA\Architect\Gateways\DockerInstance')
        );
    }
}
