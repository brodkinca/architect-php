<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Traits;

use \BCA\Architect\Architect;
use \BCA\Architect\Config;

trait Phpcs
{
    /**
     * Run PHP_Codesniffer.
     *
     * @param mixed $paths Space-separated paths to files.
     * @param array $args  Array of arguments.
     *
     * @return mixed
     */
    public function taskPhpcs($paths = '', array $args = ['interactive|i' => false])
    {
        $exec = $this->taskExec(Config::get('pathComposerBin').'/phpcs')
            ->arg('--runtime-set ignore_warnings_on_exit 1')
            ->arg('--extensions=php')
            ->arg('--report-summary')
            ->arg('--report-checkstyle='.Config::get('pathBuildDir').'/logs/checkstyle.xml');

        // Make Architect::runAll() less verbose.
        if (isset($args['run-all']) && $args['run-all']) {
            $exec->arg('-n');
        } elseif (!isset($args['run-all'])) {
            $exec->arg('--report-full');
        }

        // Show helpful sniff information?
        if (isset($args['verbose']) && $args['verbose']) {
            $exec->arg('-s');
        }

        // Set standard.
        $exec->arg('--standard='.Config::get('phpcsStandard'));

        // Set paths.
        if (empty($paths)) {
            $paths = Config::get('pathsPhpcs');
        }

        $exec->arg($paths);

        // Run interactively?
        if (isset($args['interactive']) && $args['interactive']) {
            passthru($exec->arg('-a')->getCommand(), $exitCode);

            return new \Robo\Result($exec, $exitCode, 'PHPCS coding style analysis complete.');
        }

        return $exec->run();
    }

    /**
     * Run PHP_Codesniffer Beautifier.
     *
     * @param string $paths Space-separated paths to files.
     * @param array  $args  Array of arguments.
     *
     * @return Robo\Result
     */
    public function taskPhpcbf(string $paths = '', array $args = ['interactive|i' => false])
    {
        $exec = $this->taskExec(Config::get('pathComposerBin').'/phpcbf')
            ->arg('--extensions=php')
            ->arg('--standard='.Config::get('phpcsStandard'));

        // Set paths.
        if (empty($paths)) {
            $paths = Config::get('pathsPhpcs');
        }

        $exec->arg($paths);

        // Show helpful sniff information?
        if (isset($args['verbose']) && $args['verbose']) {
            $exec->arg('-s');
        }

        return $exec->run();
    }

    /**
     * Boot phpcs trait.
     *
     * @return void
     */
    protected function bootPhpcs()
    {
        // Set defaults.
        Config::setDefault('pathsPhpcs', array_merge(
            (array) Config::get('pathsSrc'),
            (array) Config::get('pathsTests')
        ));

        Config::setDefault(
            'phpcsStandard',
            realpath(
                Config::get('pathComposerVendor').
                '/bca/phpcs-standard/src/BCA/CodingStandard/BCA'
            )
        );

        // Set weights.
        Architect::setWeight('taskPhpcs', 50);
        Architect::setWeight('taskPhpcbf', Architect::WEIGHT_NO_RUN);
    }
}
