<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Gateways;

use \BCA\Architect\Tasks\DockerInstanceMysql;
use \BCA\Architect\Tasks\DockerInstancePhp;

/**
 * Gateway to Docker instance factories.
 */
class DockerInstance
{
    /**
     * Return new DockerInstanceMysql
     *
     * @param string $tag Tag from local system or registry.
     *
     * @return DockerInstanceMysql
     */
    public function mysql(string $tag = 'mysql:latest')
    {
        return new DockerInstanceMysql($tag);
    }

    /**
     * Return new DockerInstancePhp
     *
     * @param string $tag Tag from local system or registry.
     *
     * @return DockerInstancePhp
     */
    public function php(string $tag = 'php:latest')
    {
        return new DockerInstancePhp($tag);
    }
}
