<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tasks;

use \Robo\Task\Docker\Result;
use \Robo\Task\Docker\Run;
use \BCA\Architect\Config;

/**
 * Abtract base class for Docker instance factories.
 */
abstract class DockerInstanceAbstract implements \Robo\Contract\TaskInterface
{
    use \Robo\Common\IO;
    use \Robo\Task\Base\loadTasks;
    use \Robo\Task\Base\loadShortcuts;

    /**
     * Name of docker container.
     *
     * @var string
     */
    protected $name;

    /**
     * Robo Docker\Run instance for this container.
     *
     * @var Robo\Task\Docker\Run
     */
    protected $instance;

    /**
     * Prefix for Docker container name.
     *
     * @var string
     */
    protected $prefix;

    /**
     * Suffix for Docker container name.
     *
     * @var string
     */
    protected $suffix;

    /**
     * Constructor
     *
     * @param string $tag Tag from local system or registry.
     */
    final public function __construct(string $tag)
    {
        $this->prefix = $this->getSafePrefix();
        $this->name = $this->prefix.'_'.$this->suffix;

        $this->instance = new Run($tag);
        $this->instance
            ->detached()
            ->name($this->name);
        $this->applyDefaults();
    }

    /**
     * Pass calls to unknown methods directly to instance.
     *
     * @param string $name Name of method called.
     * @param array  $args Arguments passed to called method.
     *
     * @return $this
     */
    public function __call(string $name, array $args)
    {
        call_user_func_array([$this->instance, $name], $args);

        return $this;
    }

    /**
     * Rquire that extending classes define defaults.
     *
     * @return $this
     */
    abstract protected function applyDefaults();

    /**
     * Get safe prefix for Docker container name.
     *
     * @return string
     */
    private function getSafePrefix()
    {
        $prefix = strtolower(Config::get('projectTitle'));
        $prefix = preg_replace('/\s+/', '_', $prefix);
        $prefix = preg_replace('/[^a-zA-Z0-9_]/', '', $prefix);

        return $prefix;
    }

    /**
     * Remove any Docker container with same name.
     *
     * @return boolean
     */
    final private function removeContainer()
    {
        system('docker rm -f '.$this->name, $return);

        return (bool) $return;
    }

    /**
     * Does this container exist in Docker?
     *
     * Method will return true if the any container with the same name exists
     * in Docker, whether or not it is currently running.
     *
     * @return boolean
     */
    final private function containerExists()
    {
        // Return true if container exists, not if it's running.
        return $this->_exec('docker inspect --format="{{ .State.Running }}" '.$this->name)
            ->wasSuccessful();
    }

    /**
     * Get Robo Docker instance for this container.
     *
     * @return Robo\Task\Docker\Run
     */
    final public function getInstance()
    {
        return $this->instance;
    }

    /**
     * Run Docker container and perform post-run actions.
     *
     * @return Robo\Task\Docker\Result
     */
    public function run()
    {
        if (!$this->containerExists()) {
            return $this->instance->run();
        } elseif (!getenv('CI') && $this->confirm('Container '.$this->name.' is already running. Remove?')) {
            $this->removeContainer();
            return $this->instance->run();
        }

        return new Result($this, 1, 'Container already running.', ['name' => $this->name]);
    }
}
