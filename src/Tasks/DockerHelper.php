<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tasks;

use \Robo\Task\Docker\Result;
use \Robo\Task\Base\ExecStack;

/**
 * Provides helpful methods for working with Docker containers.
 */
class DockerHelper implements \Robo\Contract\TaskInterface
{

    /**
     * Docker container ID
     *
     * @var string
     */
    private $cid;

    /**
     * Robo ExecStack instance
     *
     * @var Robo\Task\Base\ExecStack
     */
    private $execStack;

    /**
     * Constructor
     *
     * @param Robo\Task\Docker\Result $container Result for running container.
     */
    public function __construct(Result $container)
    {
        $this->execStack = new ExecStack();
        $this->cid = $container->getCid();
    }

    /**
     * Wait for mySQL server to start.
     *
     * @return DockerHelper
     */
    public function waitForMysqlToStart()
    {
        $cmd = 'bash -c \'STATUS=1; while [ $STATUS -gt 0 ]; do mysqlshow; STATUS=$?; sleep 0.1; done\'';
        $this->execStack->exec('docker exec --interactive=false '.$this->cid.' '.$cmd);

        return $this;
    }

    /**
     * Wait for webserver to start.
     *
     * @param string|integer $port Port on which server is expected to run.
     *
     * @return DockerHelper
     */
    public function waitForWebserverToStart($port)
    {
        $cmd = 'bash -c \'STATUS=1; while [ $STATUS -gt 0 ]; do curl -sf localhost:'.$port.' > /dev/null; STATUS=$?; sleep 0.1; done\'';
        $this->execStack->exec($cmd);

        return $this;
    }

    /**
     * Run queued exec commands.
     *
     * @return Robo\Result
     */
    public function run()
    {
        return $this->execStack
            ->printed(true)
            ->run();
    }
}
