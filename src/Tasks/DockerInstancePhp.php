<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tasks;

use RoboFile;
use \Robo\Task\Docker\Run;
use \BCA\Architect\Config;

/**
 * Create Robo Docker PHP instance.
 */
class DockerInstancePhp extends DockerInstanceAbstract
{

    /**
     * Suffix for Docker container name.
     *
     * @var string
     */
    protected $suffix = 'php';

    /**
     * Port on which webserver is listening.
     *
     * @var integer
     */
    protected $port;

    /**
     * Apply defaults for all mySQL containers.
     *
     * @return $this
     */
    protected function applyDefaults()
    {
        $this->instance
            ->env('ENV', 'local');

        return $this;
    }

    /**
     * Set port on which webserver will listen.
     *
     * @param string|integer $port Port on which webserver will listen.
     *
     * @return $this
     */
    public function onPort($port)
    {
        $this->port = (int) $port;
        $this->instance->publish($port, 80);

        return $this;
    }

    /**
     * Link mySQL container, matched by prefix.
     *
     * @return $this
     */
    public function withMysql()
    {
        $this->instance->link($this->prefix.'_mysql', 'mysql');

        return $this;
    }

    /**
     * Run Docker container and perform post-run actions.
     *
     * @return Robo\Task\Docker\Result
     */
    public function run()
    {
        $container = parent::run();

        if ($container->wasSuccessful() && !empty($this->port)) {
            $helper = new DockerHelper($container);
            $helper->waitForWebserverToStart($this->port)->run();
        }

        return $container;
    }
}
