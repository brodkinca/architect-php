<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tests\Gateways;

/**
 * Test \BCA\Architect\Gateways classes
 */
abstract class GatewayTestCase extends \PHPUnit_Framework_TestCase
{

    /**
     * Name of Gateway class
     *
     * @var string
     */
    protected $className;

    /**
     * Test that all public methods return tasks.
     *
     * @return void
     */
    public function testGatewayPublicMethodsOnlyReturnTasks()
    {
        $class = '\\BCA\\Architect\\Gateways\\'.$this->className;
        $class = new $class();

        $classReflector = new \ReflectionClass($class);

        foreach ($classReflector->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            $this->assertThat(
                $method->invoke($class),
                $this->isInstanceOf('\Robo\Contract\TaskInterface')
            );
        }
    }
}
