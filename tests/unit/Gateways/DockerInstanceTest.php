<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tests\Gateways;

/**
 * Test \BCA\Architect\Gateways\DockerInstance
 */
class DockerInstanceTest extends GatewayTestCase
{

    /**
     * Name of Gateway Class
     *
     * @var string
     */
    protected $className = 'DockerInstance';
}
