<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tasks;

use RoboFile;
use \Robo\Task\Docker\Run;
use \BCA\Architect\Config;

/**
 * Create Robo Docker mySQL instance.
 */
class DockerInstanceMysql extends DockerInstanceAbstract
{

    /**
     * Suffix for Docker container name.
     *
     * @var string
     */
    protected $suffix = 'mysql';

    /**
     * Apply defaults for all mySQL containers.
     *
     * @return $this
     */
    protected function applyDefaults()
    {
        $this->instance
            ->publish(3306)
            ->env('MYSQL_ALLOW_EMPTY_PASSWORD', 'yes')
            ->env('MYSQL_ROOT_PASSWORD', '')
            ->env('MYSQL_DATABASE', $this->prefix.'_db')
            ->env('MYSQL_USER', 'dbuser')
            ->env('MYSQL_PASSWORD', 'dbpass');

        return $this;
    }

    /**
     * Run Docker container and perform post-run actions.
     *
     * @return Robo\Task\Docker\Result
     */
    public function run()
    {
        $container = parent::run();

        if ($container->wasSuccessful()) {
            $helper = new DockerHelper($container);
            $helper->waitForMysqlToStart()->run();
        }

        return $container;
    }
}
