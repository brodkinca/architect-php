<?php

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \BCA\Architect\Architect
{
    use \BCA\Architect\Traits\Apigen;
    use \BCA\Architect\Traits\Codeception;
    use \BCA\Architect\Traits\Pdepend;
    use \BCA\Architect\Traits\Phpcb;
    use \BCA\Architect\Traits\Phpcs;
    use \BCA\Architect\Traits\Phpcpd;
    use \BCA\Architect\Traits\Phplint;
    use \BCA\Architect\Traits\Phpmd;
    use \BCA\Architect\Traits\Phploc;

    /**
     * Run Codeception.
     *
     * @param string $suite Name of test suite to run.
     * @param array  $args  Array of arguments.
     *
     * @return Robo\Result
     */
    public function taskTest(string $suite = null, array $args = [])
    {
        $codecept = $this->taskCodecept()
	        ->excludeGroup('dockerlib')
            ->coverage()
            ->coverageHtml()
            ->coverageXml()
            ->html()
            ->xml();

        if (!empty($suite)) {
            $codecept->suite($suite);
        }

        // Show debug information?
        if (isset($args['verbose']) && $args['verbose']) {
            $codecept->debug();
        }

        return $codecept->run();
    }
}