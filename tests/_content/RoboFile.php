<?php
// @codingStandardsIgnoreFile

/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

/**
 * Test RoboFile.
 */
class RoboFile extends \BCA\Architect\Architect
{
    use \BCA\Architect\Traits\DockerHelper;
    use \BCA\Architect\Traits\Pdepend;
    use \BCA\Architect\Traits\Phpcb;
    use \BCA\Architect\Traits\Phpcs;
    use \BCA\Architect\Traits\Phpcpd;
    use \BCA\Architect\Traits\Phplint;
    use \BCA\Architect\Traits\Phploc;
    use \BCA\Architect\Traits\Phpmd;

    public function dockerUp()
    {
        $mysql = $this->dockerInstance()->mysql('mysql:5.7')->run();
        $php = $this->dockerInstance()->php('php:5.6-apache')
            ->withMysql()
            ->onPort(8080)
            ->volume(__DIR__.'/tests/_content/docker', '/var/www/html')
            ->run();
    }
}
