<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tests\Tasks;

/**
 * Test \BCA\Architect\Tasks classes
 */
abstract class TaskTestCase extends \PHPUnit_Framework_TestCase
{

    /**
     * Test that class implements Robo's TaskInterface
     *
     * @return void
     */
    public function testTaskExtendsRoboTask()
    {
        $this->assertThat(
            $this->class,
            $this->isInstanceOf('\Robo\Contract\TaskInterface')
        );
    }
}
