<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Traits;

use \BCA\Architect\Architect;
use \BCA\Architect\Config;

trait DockerHelper
{
    /**
     * Return new DockerHelper instance.
     *
     * @param mixed $container Name of running Docker container.
     *
     * @return BCA\Architect\Tasks\DockerHelper
     */
    protected function dockerHelper($container)
    {
        return new \BCA\Architect\Tasks\DockerHelper($container);
    }

    /**
     * Return new DockerInstance instance.
     *
     * @return BCA\Architect\Tasks\DockerInstance
     */
    protected function dockerInstance()
    {
        return new \BCA\Architect\Gateways\DockerInstance();
    }
}
