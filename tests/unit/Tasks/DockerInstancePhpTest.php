<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tests\Tasks;

/**
 * Test \BCA\Architect\Tasks\DockerInstancePhp
 */
class DockerInstancePhpTest extends TaskTestCase
{

    /**
     * Instance of class under test
     *
     * @var DockerInstanceMysql
     */
    protected $class;

    /**
     * Reflector of the class under test
     *
     * @var ReflectionClass
     */
    protected $reflector;

    /**
     * Setup tests
     *
     * @return void
     */
    protected function setUp()
    {
        $this->class = new \BCA\Architect\Tasks\DockerInstancePhp('php');
        $this->reflector = new \ReflectionClass($this->class);
    }
}
