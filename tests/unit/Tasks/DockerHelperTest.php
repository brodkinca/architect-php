<?php
/**
 * BCA Architect for PHP Projects
 *
 * @package    robo
 * @subpackage bca/architect
 * @author     Brodkin CyberArts <info@brodkinca.com>
 * @copyright  2015 Brodkin CyberArts
 */

namespace BCA\Architect\Tests\Tasks;

/**
 * Test \BCA\Architect\Tasks\DockerHelper
 */
class DockerHelperTest extends TaskTestCase
{

    /**
     * Instance of the class under test
     *
     * @var DockerHelper
     */
    protected $class;

    /**
     * Reflector of the class under test
     *
     * @var ReflectionClass
     */
    protected $reflector;

    /**
     * Setup tests
     *
     * @group dockerlib
     *
     * @return void
     */
    protected function setUp()
    {
        $robo = new \RoboFile();
        $roboReflector = new \ReflectionClass($robo);

        $method = $roboReflector->getMethod('taskDockerRun');
        $method->setAccessible(true);
        $result = $method->invoke($robo, 'busybox')->run();

        $this->class = new \BCA\Architect\Tasks\DockerHelper($result);
        $this->reflector = new \ReflectionClass($this->class);
    }

    /**
     * Test that class implements Robo's TaskInterface
     *
     * @group dockerlib
     *
     * @return void
     */
    public function testTaskExtendsRoboTask()
    {
        parent::testTaskExtendsRoboTask();
    }
}
